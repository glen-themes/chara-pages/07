// how does akali get hotter with each new illustration

$(document).ready(function(){

    // load glenjamin logo
    fetch("//glen-assets.github.io/core-svgs/glenjamin.html")
    .then(glenjasvg => {
      return glenjasvg.text()
    })
    .then(glenjasvg => {
      $(".glencannotcook").html(glenjasvg)
    });

    /*-------- GENERAL ADJUSTMENTS --------*/
    $("#top-corner-text").wrapAll('<a href="/"></a>');

    if($(".badbitch").is(":visible")){
        var xx = $(".tracklist").height();
        $(".youthegoat").css("margin-top",-xx)
    }

    var boxw = $(".youthegoat").height();
    $(".youthegoat").height(boxw);

    if(!$(".glenjamin").is(":visible")){
        divvy = $("<a class='worm' href='//glenthemes.tumblr.com' title='coded by glenthemes'>glenthemes</a>");
        $("body").prepend(divvy);
        $(".worm").css({"position":"fixed",
                        "bottom":"0",
                        "margin-bottom":"var(--Screen-Margin)",
                        "right":"0",
                        "margin-right":"var(--Screen-Margin)",
                        "font-family":"abel",
                        "font-size":"11px",
                        "text-transform":"uppercase",
                        "letter-spacing":"1px",
                        "color":"var(--Indicator-Color)",
                        "line-height":"1em",
                        "opacity":"0.8",
                        "z-index":"1"
                      })
    }

    var uu = $(".watermalone").width();
    $(".watermalone").width(uu);


    /*------ STATS N SHIT ------*/

    /*felix was here*/
    var incval = parseInt(getComputedStyle(document.documentElement)
                .getPropertyValue("--Stats-Slant-Amount"));
    /*finds visible tab on pageload*/
    var visibleTab = $(".stats:visible").attr("chara-id");

    /*felix was here*/
    /*function w/ incval & visible tab arg*/
    function felix(incval, visible) {
      var current = 0;

      /*.onestat s only under current visible chara-id*/
      $(".stats[chara-id=" + visible + "] .onestat").each(function(i){

        $(this).css("margin-right", i * incval);

        $(this).css("transition-delay", current + "s");
        $(this).css("opacity","1");
        current++;
        current = current - 0.9;
      });
    }

    /*felix was here*/
    /*call the function on pageload with visibleTab as previously established*/
    felix(incval, visibleTab);

    /*-- for the tumblr editor only --*/
    /*-- issue: felix() does not run. force it to run once --*/
    t = setInterval(function(){
      if ($(".onestat").css("opacity","0")) {
        $(".onestat").css("opacity","1");
        clearInterval(t);
      }
    },0);


    $(".stats").each(function(){
        $(this).find(".onestat:last").addClass("holyshit");
    });

    var cookie = parseInt(getComputedStyle(document.documentElement)
                .getPropertyValue("--Stat-Detail-Offset-Amount"));

    var eve = parseInt($(".holyshit").css("margin-right"));
    var bush = eve+cookie;

    $(".chara-listing").css("margin-right",bush + "px");

    var trans_speed = parseInt(getComputedStyle(document.documentElement)
                    .getPropertyValue("--Character-Transition-Speed"));

    /*---- get transition delay value of last bitch ----*/
    var king = $(".holyshit").css("transition-delay"),
        qing = king.split("s")[0],
        undec = Math.floor((qing * 1000) + (trans_speed * 2));


    /*-------- CHARACTER SELECTION --------*/
    $(".one-chara-icon:first").addClass("i-active");

    // hover effect - show frame on hover
    $(".one-chara-icon").hover(function(){
        $(this).addClass("i-hover");
        $(".i-active").addClass("fog").removeClass("i-active");
    }, function(){
        $(this).removeClass("i-hover");
        $(".fog").addClass("i-active").removeClass("fog");
    });

    // click - activate the frame
    $(".one-chara-icon").each(function(){
        $(document).on("click", ".i-hover", function(){
            $(this).siblings().removeClass("i-active fog").find(".vanillae").removeClass("vc");
            $(this).addClass("i-active");
            $(this).find(".vanillae").addClass("vc");

            $(".one-chara-icon").addClass("pen");

            setTimeout(function(){
                $(".one-chara-icon").removeClass("pen");
            },undec);
        });
    });

    // click - ACTUALLY change the character panels
    // felix (@nonspace) helped me A LOT with this part. can i kiss u pls ty
    $(".one-chara-icon").click(function(){

        var haha = $(this).attr("chara-id");
        var getID = '[chara-id="' + haha + '"]';

        if($(".stats, .olive, .infoholder").filter(getID).is(":hidden")){
            /*felix was here*/
            /*resets the opacity of all stats AFTER fadeOut*/
            $(".stats, .olive, .infoholder").fadeOut(trans_speed, function(){
              $(".stats .onestat").css("opacity","");
            });
        }

        if($(".stats, .olive, .infoholder").has("[chara-id]")){
            /*felix was here*/
            /*AFTER new tab faded in, calls opacity fn w/ current chara-id*/
            $(".stats, .olive, .infoholder").filter(getID).delay(trans_speed).fadeIn(trans_speed, function(){
              felix(incval, haha);
            });
        }

        if ($(".stats, .olive, .infoholder").filter(getID).length == 0) {
            $(".stats, .olive, .infoholder").fadeOut(trans_speed);
        }
    });//end button click


    /*-------- CHARACTER IMAGE SETTINGS --------*/
    var zz = $(".lemon").width();
    $(".lemon").width(zz);

    $(".chara-image").each(function(){
        if($(this).attr("height") || $(this).attr("width")){
            $(this).css({"max-height":"none",
                                  "max-width":"none"
                        });
        }

        if($(this).attr("top")){
            var getTop = $(this).attr("top");
            $(this).css({"top":"0",
                         "margin-top":getTop
                        });
        }

        if($(this).attr("bottom")){
            var getBot = $(this).attr("bottom");
            $(this).css({"bottom":"0",
                         "margin-bottom":getBot
                        });
        }

        if($(this).attr("left")){
            var getLeft = $(this).attr("left");
            $(this).css({"left":"0",
                         "margin-left":getLeft
                        });
        }

        if($(this).attr("right")){
            var getRight = $(this).attr("right");
            $(this).css({"right":"0",
                         "margin-right":getRight
                        });
        }
    });//end charaimage each


    /*-------- SIGNATURE IMAGE SETTINGS --------*/
    $(".signature-image").each(function(){
        if($(this).attr("top") === ""){
            $(this).css("top","0");
        }

        if($(this).attr("bottom") === ""){
            $(this).css("bottom","0");
        }

        if($(this).attr("left") === ""){
            $(this).css("left","0");
        }

        if($(this).attr("right") === ""){
            $(this).css("right","0");
        }

        if($(this).attr("top")){
            var getTop = $(this).attr("top");
            $(this).css({"top":"0",
                         "margin-top":getTop
                        });
        }

        if($(this).attr("bottom")){
            var getBot = $(this).attr("bottom");
            $(this).css({"bottom":"0",
                         "margin-bottom":getBot
                        });
        }

        if($(this).attr("left")){
            var getLeft = $(this).attr("left");
            $(this).css({"left":"0",
                         "margin-left":getLeft
                        });
        }

        if($(this).attr("right")){
            var getRight = $(this).attr("right");
            $(this).css({"right":"0",
                         "margin-right":getRight
                        });
        }
    });//end sig each


    /*-------- CHARACTER NAME OUTLINE(s) --------*/
    $(".chara-name span").each(function(){
        $(this).clone().addClass("clone-1").appendTo($(this).parent());
        $(this).clone().addClass("clone-2").appendTo($(this).parent());
    });


    $(".ob-fill").each(function(){
        if($(this).attr("fill")){
            var fillAmount = $(this).attr("fill");
            $(this).css("width",fillAmount);
        }
    });


    /*---------------- MUSIC PLAYER ----------------*/
    $(".song:first").addClass("current");

    $(".prev").click(function(){
        $(".song").filter(".current").hide().removeClass("current").prev(".song").show().addClass("current");
        if(!$(".song").hasClass("current")){
            $(".song:last").addClass("current").show();
        }
    });//end prev

    $(".next").click(function(){
        $(".song").filter(".current").hide().removeClass("current").next(".song").show().addClass("current");
        if(!$(".song").hasClass("current")){
            $(".song:first").addClass("current").show();
        }
    });//end next

    // when each song ends, jumps to the next one
    $(".song").find("audio").each(function(){
        $(this).bind("ended", function(){
            $(".song").filter(".current").hide().removeClass("current").next(".song").show().addClass("current");
            var uhhh = $(".current").find("audio")[0];
            if (uhhh.paused) {
                uhhh.play();
                $(".pausee").show();
                $(".playy").hide();
            } else {
                uhhh.pause();
                $(".playy").show();
                $(".pausee").hide();
            }
        });
    });

    // when playlist has finished, stop all music and show first song
    $(".song:last").find("audio")[0].onended = function(){
        $(".playy").show();
        $(".pausee").hide();

        $(".song").filter(".current").hide().removeClass("current").next(".song").show().addClass("current");
        if(!$(".song").hasClass("current")){
            $(".song:first").addClass("current").show();
        }
    }

    $(".prev, .next, .playbutt").click(function(){
        var uhhh = $(".current").find("audio")[0];

        if (uhhh.paused) {
            uhhh.play();
            $(".pausee").show();
            $(".playy").hide();
        } else {
            uhhh.pause();
            $(".playy").show();
            $(".pausee").hide();
        }

        uhhh.addEventListener("timeupdate", function(){
            var quoi = parseInt(((uhhh.currentTime / uhhh.duration) * 100), 10) + "%";
            $(".barfill").css("width",quoi)
        });
    });//end click

    $(".brococho").click(function(T_T){
        var uhhh = $(".current").find("audio")[0];
        var pin = (T_T.pageX - $(".badbitch").offset().left - this.offsetLeft) / this.offsetWidth,
            point = pin * uhhh.duration;
        uhhh.currentTime = point;

        uhhh.addEventListener("timeupdate", function(){
            var quoi = parseInt(((uhhh.currentTime / uhhh.duration) * 100), 10) + "%";
            $(".barfill").css("width",quoi)
        });
    });


    $(".volbar").click(function(T_T){
        var uhhh = $(".current").find("audio")[0];
        var uvu = (T_T.pageX - $(".badbitch").offset().left - this.offsetLeft) / this.offsetWidth;

        $("audio").each(function(){
            this.volume = uvu;
        });

        var tofu = uvu * 100;
        $(".volfill").css("width",tofu + "%");

        $(".banyasuo").text(uvu);

        if(tofu < 50){
            $(".mute, .max").hide();
            $(".low").show();
        }

        if(tofu >= 50){
            $(".mute, .low").hide();
            $(".max").show();
        }
    });


    $(".max, .low").click(function(){
        var uhhh = $(".current").find("audio")[0];
        $(this).hide();
        $(".mute").show();
        uhhh.volume = 0;
        $(".volfill").css("width","0%");
    });


    $(".mute").click(function(){
        var uhhh = $(".current").find("audio")[0];
        $(this).hide();
        $(".max").show();

        var rev = $(".banyasuo").text();
        var res = rev * 100;

        uhhh.volume = rev;
        $(".volfill").css("width",res + "%");

        if(rev === ""){
            uhhh.volume = 1;
            $(".volfill").css("width","100%");
        }
    });

    document.addEventListener("play", function(e){
    var audios = document.getElementsByTagName("audio");
        for(var i = 0, len = audios.length; i < len;i++){
            if(audios[i] != e.target){
                audios[i].pause();
                audios[i].currentTime = 0;
            }
        }
    }, true);

    $(".youthegoat").css("opacity","1");

});//end ready
